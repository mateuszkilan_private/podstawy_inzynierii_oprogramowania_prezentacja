ulimit -n 5000 # ta komenda wyświetla limit file descriptoró per proces
lsof | wc -l # oblicza ilość aktualnie wykorzystanych file descriptorów
cat /proc/sys/fs/file-max # wyświetla maksymalną ilość file descriptorów do wykorzystania przez system operacyjny

ulimit -n 2048 # ta komenda zmienia limit file descriptorów na 2048 per proces
echo 1000000 >> /proc/sys/fs/file-max # ta komenda ustawia maksymalną ilość file descriptorów dla całego systemu na jeden milion
