# ifstream i ofstream

Wykorzystanie dwóch uchwytów do korzystania z pliku nie jest problemem tak długo jak nie są to dwa uchwyty w trybie do zapisu. W takiej sytuacji problem polega na tym, że oba uchywyt dopisują dane do pliku zaczynając od tego samego miejsca. Rezultat jest taki, że drugi uchwyt nadpisuje informacje dodane przez uchwyt pierwszy.

## ofstream

klasa ofstream służy tylko i wyłącznie do zapisywania informacji do pliku.

klasa ofstream - posiada zawsze ustawioną flagę trybu ios_base::out, niezależnie od tego jaką flagę trybu pracy ustawimy. W przypadku skorzystania z konstruktora i nie podania trybu pracy na pliku domyślnie zawsze zostanie użyty ios_base::out.

tryb ios_base::out domyślnie otwiera plik do zapisu i automatycznie czyści zawartość całego pliku, aby do tego nie doszło zachodzi konieczność skorzystania z dodatkowej flagi trybu pracy czyli ios_base::in. Najciekawsze w tym wszystkim jest to, że konieczność wykorzystania flagi ios_base::in do niewyczyszczenia pliku podczas otwierania, nie daje nam możliwości czytania z pliku. Dodatkowo jeżeli chcemy rozpocząć dopisywanie nowych informacji do końca pliku istnieje potrzeba skorzystania z trzeciego trybu pracy czyli ios_base::ate. W przeciwnym wypadku podczas pierwszej operacji zapisu do pliku automatycznie wyczyści nam całą zawartość.

Ustawienie tych trzech flag umożliwia:
- Otworzenie pliku w trybie do zapisu
- Nieczyszczenie pliku podczas otwierania
- Dopisywanie nowych informacji bezpośrednio na końcu pliku

## ifstream

klasa ifstream służy tylko i wyłącznie do odczytywania informacji z pliku.

klasa ifstream - posiada zawsze ustawioną flagę trybu ios_base::in, niezależnie od tego jaką flagę trybu pracy ustawimy. W przypadku skorzystania z konstruktora i nie podania trybu pracy na pliku, domyślnie zawsze zostanie użyty ios_base::in.

Z ciekawostek dodam, że ifstream ignoruje ustawienie flagi ios_base::out, czyli nie uda nam się ustawić trybu zapisu do pliku. Z dodatkowych ciekawostek, ustawienie flagi ios_base::trunc czyści nie zawartość pliku jak można by założyć a sam bufor, który zostanie wczytany do pamięci. Dlaczego? dlatego, że ifstream to uchwyt nie dający możliwość wykonania operacji zapisu do pliku.

# fstream

Klasa fstream łączy w sobie funkcjonalność klas ifstream i ofstream. Może jednocześnie zajmować się zarówno zapisem jak i odczytem danych z pliku w zależności od ustawionych flag trybu pracy.

```cpp
file.seekp(3);
if (file.fail()) cout << "out pointer move failed";
file << " ";
file.write(&two[0], 3);
```
Podczas programistycznych operacji na plikach nie można robić tak jak robimy pracując na w zwykłych edytorach tekstu, czyli wrzucać nowe znaki pomiędzy dwa istniejące znaki. W tym przypadku trzeba ręcznie przesunąć wskaźnik 'out' na rządaną pozycję, po czym sprawdzamy poprawność operacji przesunięcia. Następnie wrzucamy brakującą spację i ponownie wrzucamy ciąg "two".

```cpp
file.tellg() && file.seekg(0);
```
W tym przypadku na początek wywołujemy metodę tellg(), ta metoda pozwala nam na otrzymanie indeksu pozycji wskaźnika 'in'.

Następnie jeżeli ta metoda zwróci wartość rzutowalną na true, to użyty zostanie mechanizm 'short circuting' i odpali się metoda seekg(0), która odpowiedzialna jest za ustawienie wskaźnika 'in' na wskazanym indeksie.

W zasadzie jedyny moment kiedy seekg(0) się nie odpali będzie w sytuacji kiedy ten wskaźnik już będzie na pozycji zerowej. Wynika to z tego, że liczby ujemne (sugerujące błąd w przypadku zwrócenia przez -1) również są traktowane jako truthy.

```cpp
file.getline(lineCharArray, sizeof(lineCharArray));
file.seekg(0);
getline(file, lineString);
```

W tym przypadku widzimy dwie metody getline. Jedna wywoływana jest na instancji klasy fstream i przyjmuje dwa argumenty, pierwszy to jest wskaźnik na char, drugi zaś to długość w bajtach jaką mamy sczytać i zapisać do miejsca wskazanego przez wskaźnik na char. Niestety ale ta wersja getline nie posiada przeładowania umożliwijącego w łatwy sposób wykorzystać std::string jako pierwszy argument.

Aby móc zapisać dane do std::string, musimy skorzystać z wersji metody przypisanej do standardowej przestrzeni nazw czyli std::getline. Ta wersja metody przyjmuje jako pierwszy argument uchwyt do pliku a jako drugi std::string do którego chcemy sczytać dane.

Ważne do podkreślenia jest to, że obie wersje jako trzeci argument przyjmuja znak limitujacy czytanie z pliku.

```cpp
file.seekg(0 , ios_base::end);
```
Tutaj widzimy drugie przeładowanie metody seekg, podającego ios_base::end jako drugi parameter. W tej sytuacji oznacza to, że określamy pozycje pierwszym argumentem względem końca pliku.

```cpp
file.close();
```
W tym przypadku kończymy operacje na pliku. Ta metoda wywołuje m.in. file.flush(), która powoduje przekopiowanie zawartości z bufora do pliku.

Domyślnie w trakcie pracy na pliku, bufor zrzuca informacje do pliku za każdym razem jak pamięć bufora się skończy.

# Tryb ios_base::binary

Alternatywą do korzystania z danych zapisywanych przy użyciu danych tekstowych są dane zapisywane do pliku w formacie binarnym.

W tym celu istnieje specjalny tryb pracy na plikach **ios_base::binary**.

Tryb binarny posiada pare zalet i wad w porównaniu do plików tekstowych.

**Zalety**

Zalety są takie, że zazwyczaj pliki binarne zajmują mniej miejsca na dysku.
Dodatkowo operacje na plikach w trybie binarnym są szybsze ze względu na brak konieczności wykonywania konwersji.

**Wady**

Wady są takie, że pliki w trybie binarnym są zupełnie nieczytelne dla ludzi.