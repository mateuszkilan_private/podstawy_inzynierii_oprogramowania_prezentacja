string untilWhiteChar = "", two = "two";
char lineCharArray[32];
string lineString;

fstream file;
file.open(
    "example.txt", 
    ios::in | ios::out |
    ios::trunc | ios::ate
);

if (!file.good()) return 0;

file.write("one", 3);   // 'one'
file.write(&two[0], 3); // 'onetwo'
file.seekp(3); 
if (file.fail())
    cout << "out pointer move failed";
file << ' '; // 'one '
file.write(&two[0], 3); // 'one two'
file << " three"; // 'one two three'

file.tellg() && file.seekg(0);
file >> untilWhiteChar; // 'one'
file.getline(
    lineCharArray,
    sizeof(lineCharArray)
); // ' two three'
file.seekg(0);
getline(
    file, lineString
); // 'one two three'
file.seekg(0 , ios_base::end);
file.close(); // calls file.flush()


#include <iostream>
#include <fstream>

using namespace std;

int main() {
    
    string firstPart, secondPart;

    ofstream outFile("example.txt");
    ifstream inFile("example.txt");

    if (!outFile || !inFile) {
        return -1;
    }

    outFile << "FirstPart SecondPart";

    outFile.close();

    inFile >> firstPart;
    inFile >> secondPart;
    inFile.close();

    cout << firstPart << '\n';
    cout << secondPart << '\n';

    return 0;
}

if (file.good()) cout << "last operation was successfull";

#include <fstream>

// Będziemy omawiać 

fstream file("example.txt", ios_base::in | ios_base::out);
ofstream outFile("example.txt", ios_base::out);
ifstream inFile("example.txt", ios_base::in);


ios_base::in         // Otworzenie pliku do odczytu
ios_base::out       // Otworzenie pliku do zapisu
ios_base::binary  // Operacje wykonywane są w trybie binarnym
ios_base::ate       // Ustawia wskaźnik pozycji na końcu pliku
ios_base::app      // Ustawia wskaźnik pozycji na końcu pliku
                             // Dane mogą być zapisywane tylko na końcu pliku
ios_base::trunc    // Zawartość pliku jest czyszczona


int main() {
    string untilWhiteChar = "", two = "two";
    char lineCharArray[32];
    string lineString;

    fstream file;
    file.open("example.txt", ios::in | ios::out | ios::trunc | ios::ate);

    if (!file.good()) return 0;

    file.write("one", 3);   // 'one'
    file.write(&two[0], 3); // 'onetwo'


    file.seekp(3); // podczas operacji na plikach nie można robić tak jak robi się w edytorach 
                          // tekstu, czyli wciskać tekst pomiędzy
    if (file.fail()) cout << "out pointer move failed"; // operacja przesunięcia wskaźnika dla wyjścia
                                                        // do pliku może się nie udać!
    file << ' ';            // 'one '
    file.write(&two[0], 3); // 'one two'
    file << " three";       // 'one two three'

    file.tellg() && file.seekg(0); // 13 , dlatego że wskaźnik dla 'in' porusza się razem z 'out'
    file.seekg(0);
    file >> untilWhiteChar; // 'one'
    file.getline(lineCharArray, sizeof(lineCharArray)); // ' two three' fstream::getline ma problemy
                                                        // ze wsparciem dla string
    
    file.seekg(0);

                   // nie używam file.tellg() dlatego, że wskaźnik 'in' jest równy -1, a wynika to z tego,
                   // że doszedł do końca pliku.
                   // wartości wskaźnika 'in' i 'out' mogą się różnić tylko i wyłącznie w sytuacji kiedy któraś z operacji odczyt/zapis zgłosi problem
                   // rozwiązaniem problemu mogłoby się wydawać możliwość skorzystania z trzeciego argumentu ios::getline czyli znaku limitujacego działanie
                   // getline, problemem w tej sytuacji natomiast jest to że nie istnieje znak przedstawiający koniec pliku.
    getline(file, lineString); // 'one two three'
    file.seekg(0 , ios_base::end);          // wracamy zarówno wskaźnikami 'in' i 'out' na sam koniec pliku
    file.close(); // odpala m.in. file.flush() czyli zrzuca wszystko z bufora do pliku


    // cout << untilWhiteChar << '\n';
    // cout << lineCharArray << '\n';
    // cout << lineString << '\n';
    // file.close(); // odpala file.flush() czyli zrzuca wszystko z buffora do pliku
}

    fstream binaryFile("binaryFile.txt", 
        ios_base::out | ios_base::binary | ios_base::in | ios_base::trunc
    );
    fstream textFile("textFile.txt", ios_base::out | ios_base::trunc);
    if (!binaryFile || !textFile) return 0;

    long int number = 123456789012345; // 48 bits / 6 bytes
    cout << sizeof(long int) << '\n';  // 64 bits / 8 bytes
    long int numberFromBinFile = 0;

    // binaryFile.write((char*)&number, sizeof(number));
    binaryFile.write((char*)&number, 6);
    textFile << number;

    binaryFile.seekp(0);
    binaryFile.seekg(0);

    binaryFile.read((char*)&numberFromBinFile, 6);
    cout << numberFromBinFile << '\n';

    binaryFile.close();
    textFile.close();



// Podejscie proceduralne ponizej

r     // otworzenie pliku w trybie do odczytu. Plik musi istnieć.
w   // tworzy nowy plik w trybie do zapisu. Jeżeli takowy plik istnieje
      // to czyści jego zawartość i traktuje go jak nowy plik.
a    // otworzenie pliku w trybie do zapisu. Wskaźnik pozycji jest
      // ustawiany na sam koniec pliku, jakiekolwiek ręczne próby zmiany
      // wskaźnika pozycji są ignorowane. Jeżeli plik nie istnieje to jest tworzony
r+   // otworzenie pliku w trybie do odczytu i zapisu. Plik musi istnieć
w+  // otworzenie pliku w trybie do odczytu i zapisu. Tworzy nowy plik. Jeżeli
       // taki plik istnieje to jego zawartość jest kasowana
a+   // otworzenie pliku w trybie do odczytu i zapisu. Wszystkie operacje zapisu
       // do pliku umieszczają dane zawsze na końcu pliku. Wskaźnik pozycji
       // w przypadku odczytu poruszać się może po całym pliku. W przypadku
       // skorzystania z operacji zapisu do pliku wskaźnik pozycji zawsze ustawia
       // się na końcu pliku.

FILE* pointerToFile;
pointerToFile = fopen("someFile.txt", "r");

fclose(pointerToFile);

#include <fstream>

#include <stdio.h>

file.flush();

file.close();

    char lineCharArray[32];
    file.getline(lineCharArray, sizeof(lineCharArray));

    long int numberFromBinFile = 0;
    binaryFile.read((char*)&numberFromBinFile, 6);

    string lineString;
    fstream file;
    file.open("example.txt", ios::in | ios::out | ios::trunc | ios::ate);
    getline(file, lineString);





#include <stdio.h>
#include <ctype.h>
#include <iostream>


using namespace std;

int main()
{
    char c = 0;
    int ansiCode = 0;
    char cArr[15];

    FILE *fin, *fout;

    fin = fopen( "data.txt", "rt" );
    fout = fopen( "results.txt", "wt" );

    if (fin && fout) {
        // while (true) {
        //     c = fgetc(fin);
        //     ansiCode = static_cast<int>(c);
        //     if (ansiCode == -1 || ansiCode < 0 || ansiCode > 255) break; // if fgetc function got to the end of file it returns -1
        //     c = tolower(c);
        //     fputc(c, fout);
        // }
        for (;;) {
            c = fgetc(fin);
            ansiCode = static_cast<int>(c);

            if (feof(fin)) {
                break;
            }

            if (ansiCode < 0 || ansiCode > 255) {
                continue;
            }

            c = tolower(c);
            fputc(c, fout);
        }
        // while( !feof(fin) ){
        //     c = fgetc(fin);
        //     c = tolower(c);
        //     // int ansiCode = static_cast<int>(c);
        //     // ansiCode >= 0 && ansiCode <= 255 && fputc(c, fout);
        //     fputc(c, fout);
        // }
        rewind(fin);
        fread(cArr, sizeof(char), 14, fin);

        cArr[14] = '\0';

        cout << '\n' << cArr << '\n';

        fseek(fout, 0, SEEK_END);
        fwrite(cArr, sizeof(char), 14, fout);
    }

    fclose( fin );
    fclose( fout );
    return 0;
}

char cArr[17] = "working on files";
char cArr[16] = '\0';
fwrite(cArr, sizeof(char), 16, fileOutput);
